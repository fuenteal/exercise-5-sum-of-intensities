# Exercise 5 - Sum of intensities

Exercise 5 of the sample exam: Write a program to plot images that show the difference between the probability
density of the sum of two intensities and the probability density of one intensity.